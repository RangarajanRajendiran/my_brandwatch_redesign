import logging
import os

class LogManager:
	"""
	A class to represent a Log Manager.

	...

	Attributes
	----------
	logger : str
	    contains the handle for the logger

	Methods
	-------
	createLogger():
	    Creates the logger for Log Manager

	getLogger():
	    Gets the logger for Log Manager

	setLogger():
	    Sets the logger for Log Manager

	deleteLogger():
	    Deletes the logger for Log Manager

    """	
	def __init__(self, plugin):
		self.logger = ""
		self.logfile = plugin + ".log"
		self.plugin = plugin


	def createLogger(self, loglevel = logging.INFO):
		logfile = self.logfile
        # Gets or creates a logger
		logger = logging.getLogger(self.plugin)     

		if os.path.exists(logfile):
		    open(logfile, 'w').close()

		if not logger.hasHandlers():        
		    # set log level
		    logger.setLevel(loglevel)        
		    
		    # define file handler and set formatter
		    file_handler = logging.FileHandler(logfile)
		    formatter    = logging.Formatter('[%(filename)s - %(funcName)s() : %(lineno)s ] %(asctime)s : %(levelname)s : %(message)s')
		    #formatter    = logging.Formatter('%(asctime)s : %(levelname)s : %(message)s')
		    file_handler.setFormatter(formatter)
		    
		    # add file handler to logger
		    logger.addHandler(file_handler)

		logger.debug("Logger : {}".format(logger))
		logger.debug("LogManager --- createLogger --- End")    
		self.logger = logger  
		

	def getLogger(self):
		self.logger.debug("LogManager --- getLogger --- Start")
		self.logger.debug("LogManager --- getLogger --- End")
		return self.logger
		


	def setLogger(self, logger):
		self.logger.debug("LogManager --- setLogger --- Start")
		self.logger = logger
		self.logger.debug("LogManager --- setLogger --- End")

	def setLoggerLevel(self, loglevel):
		self.logger.debug("LogManager --- setLoggerLevel --- Start")
		self.logger.setLevel(loglevel)
		self.logger.debug("LogManager --- setLoggerLevel --- End")		


	def deleteLogger(self):
		self.logger.debug("LogManager --- deleteLogger --- Start")
		handlers = self.logger.handlers[:]
		for handler in handlers:
		    handler.close()
		self.logger.removeHandler(handler)
		self.logger.debug("LogManager --- deleteLogger --- End")