import logging 

from logmanager import LogManager
from plugin import Plugin

plugin_name = "brandwatch"

def main():
	branchwatch = Plugin(plugin_name)
	branchwatch.createFlow()
	branchwatch.startFlow()
	branchwatch.stopFlow()
	branchwatch.deleteFlow()

if __name__ == '__main__': 
	main()
