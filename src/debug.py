from pprint import pprint
from pprint import pformat
import logging

class Debug:
	def __init__(self, config):
		self.config = config
		self.log_manager = self.config.log_manager
		self.logger = self.log_manager.getLogger()
		self.logger.debug("Debug --- __init__ --- Start")
		# Debug Level
		# 0 - No Debug Info
		# 1 - Basic Debug Info
		# 2 - Advanced Debug Info
		self.debug_level = self.config.debug_level

		self.logger.debug("Debug --- __init__ --- End")


	def createDebug(self):
		self.logger.debug("Debug --- createDebug --- Start")

		if self.debug_level == 0:
			self.config.log_manager.setLoggerLevel(logging.INFO)
		else:
			self.config.log_manager.setLoggerLevel(logging.DEBUG)

		self.logger.debug("Debug --- createDebug --- End")


	def startDebug(self):
		self.logger.debug("Debug --- startDebug --- Start")

		# Basic Debug Info
		if self.debug_level >= 1:
			self.config.log_manager.setLoggerLevel(logging.DEBUG)
			self.dumpConfig()

		# Advanced Debugging
		if self.debug_level >= 2:
			pass			

		self.logger.debug("Debug --- startDebug --- End")


	def stopDebug(self):
		self.logger.debug("Debug --- stopDebug --- Start")
		self.logger.debug("Debug --- stopDebug --- End")


	def deleteDebug(self):
		self.logger.debug("Debug --- deleteDebug --- Start")
		self.logger.debug("Debug --- deleteDebug --- End")

	
	def dumpConfig(self):
		self.logger.debug("Debug --- dumpConfig --- Start")
		config = self.config.getConfig()
		self.logger.debug("\n\n")
		self.logger.debug("Dumping Basic Plugin Configuration")
		self.logger.debug("\n" + pformat(config))
		self.logger.debug("\n\n")
		self.logger.debug("Debug --- dumpConfig --- End")
