class Process:
	def __init__(self, config):
		self.config = config
		self.log_manager = config.log_manager
		self.logger = self.log_manager.getLogger()
		self.logger.debug("Process --- __init__ --- Start")
		self.logger.debug("Process --- __init__ --- End")


	def createProcess(self):
		self.logger.debug("Process --- createProcess --- Start")
		self.logger.debug("Process --- createProcess --- End")


	def startProcess(self):
		self.logger.debug("Process --- startProcess --- Start")
		self.logger.debug("Process --- startProcess --- End")


	def stopProcess(self):
		self.logger.debug("Process --- stopProcess --- Start")
		self.logger.debug("Process --- stopProcess --- End")


	def deleteProcess(self):
		self.logger.debug("Process --- deleteProcess --- Start")
		self.logger.debug("Process --- deleteProcess --- End")