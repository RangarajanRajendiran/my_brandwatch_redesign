import logging

class Configuration:
	"""
	A class to represent a Configuration.

	...

	Attributes
	----------
	data : dict
	    contains all the configuration details of the plugin

	Methods
	-------
	readConfig():
	    Reads the configuration of the plugin

	writeConfig():
	    Writes the configuration of the plugin

	updateConfig():
	    Updates the configuration of the plugin

	deleteConfig():
	    Deletes the configuration of the plugin

	getConfig():
	    Get the configuration of the plugin

    """


	def __init__(self):
		# Basic Configuration
		self.log_manager = ""
		self.logger = ""
		#self.log_level = logging.DEBUG
		# Debug Level
		# 0 - No Debug Info
		# 1 - Basic Debug Info
		# 2 - Advanced Debug Info		
		self.debug_level = 2

		# gui user input 
		self.data = {}


	def readConfig(self):
		'''
		Returns configuration of the plugin.

			Parameters:
			----------
			None

			Returns:
			-------
			data : dict
				returns the configuration of the plugin

		'''
		self.logger = self.log_manager.getLogger()
		self.logger.debug("Configuration --- readConfig --- Start")

		self.data['username'] = ""
		self.data['password'] = ""
		self.data['api_token'] = ""
		self.data['brandwatch_schema'] = ""
		self.data['brandwatch_schema_readable_format'] = ""
		self.data['brandwatch_final_schema'] = ""
		self.data['order_dir'] = ""
		self.data['page_size'] = ""
		self.data['start_date'] = ""
		self.data['end_date'] = ""
		self.data['brandwatch_query'] = ""

		self.logger.debug("Configuration --- readConfig --- End")

	def writeConfig(self):
		self.logger.debug("Configuration --- writeConfig --- Start")
		self.logger.debug("Configuration --- writeConfig --- End")


	def updateConfig(self, param, value):
		self.data[param] = value


	def deleteConfig(self, param):
		self.logger.debug("Configuration --- deleteConfig --- Start")
		if self.data.get(param):
			self.data.pop(param)
			self.logger.debug("Parameter removed : {}".format(param))
		else:
			self.logger.debug("Parameter not found : {}".format(param))
		self.logger.debug("Configuration --- deleteConfig --- End")


	def getConfig(self):
		self.logger.debug("Configuration --- getConfig --- Start")
		self.logger.debug("Configuration --- getConfig --- End")
		return vars(self)
		