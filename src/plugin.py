import logging

from configuration import Configuration
from logmanager import LogManager
from input import Input
from output import Output
from process import Process
from debug import Debug

class Plugin:
	def __init__(self, plugin_name):
		self.log_manager = ""
		self.logger = ""
		self.config = ""
		self.input = ""
		self.process = ""
		self.output = ""
		self.debug = ""
		self.plugin_name = plugin_name


	def initializeModules(self):
		# Get basic configurations
		self.config = Configuration()

		# Log Manager Creation
		self.log_manager = LogManager(self.plugin_name)
		self.config.log_manager = self.log_manager
		self.log_manager.createLogger()
		self.logger = self.log_manager.getLogger()		

		self.input = Input(self.config)
		self.process = Process(self.config)
		self.output = Output(self.config)
		self.debug = Debug(self.config)

		self.logger.debug("Plugin --- initializeModules --- End")


	def createModules(self):
		self.logger.debug("Plugin --- createModules --- Start")

		self.config.readConfig()
		self.input.createInput()
		self.process.createProcess()
		self.output.createOutput()
		self.debug.createDebug()

		self.logger.debug("Plugin --- createModules --- End")


	def createFlow(self):
		

		self.initializeModules()
		self.createModules()

		self.logger.debug("Plugin --- createFlow --- End")


	def startFlow(self):
		self.logger.debug("Plugin --- startFlow --- Start")

		self.input.startInput()
		self.process.startProcess()
		self.output.startOutput()
		self.debug.startDebug()

		self.logger.debug("Plugin --- startFlow --- End")


	def stopFlow(self):
		self.logger.debug("Plugin --- stopFlow --- Start")

		self.input.stopInput()
		self.process.stopProcess()
		self.output.stopOutput()
		self.debug.stopDebug()

		self.logger.debug("Plugin --- stopFlow --- End")


	def deleteFlow(self):
		self.logger.debug("Plugin --- deleteFlow --- Start")

		self.input.deleteInput()
		self.process.deleteProcess()
		self.output.deleteOutput()
		self.debug.deleteDebug()
		
		self.logger.debug("Plugin --- deleteFlow --- End")