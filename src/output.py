class Output:
	def __init__(self, config):
		self.config = config
		self.log_manager = config.log_manager
		self.logger = self.log_manager.getLogger()
		self.logger.debug("Output --- __init__ --- Start")
		self.logger.debug("Output --- __init__ --- End")


	def createOutput(self):
		self.logger.debug("Output --- createOutput --- Start")
		self.logger.debug("Output --- createOutput --- End")


	def startOutput(self):
		self.logger.debug("Output --- startOutput --- Start")
		self.logger.debug("Output --- startOutput --- End")


	def stopOutput(self):
		self.logger.debug("Output --- stopOutput --- Start")
		self.logger.debug("Output --- stopOutput --- End")


	def deleteOutput(self):
		self.logger.debug("Output --- deleteOutput --- Start")
		self.logger.debug("Output --- deleteOutput --- End")