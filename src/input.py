class Input:
	def __init__(self, config):
		self.config = config
		self.log_manager = config.log_manager
		self.logger = self.log_manager.getLogger()
		self.logger.debug("Input --- __init__ --- Start")
		self.logger.debug("Input --- __init__ --- End")


	def createInput(self):
		self.logger.debug("Input --- createInput --- Start")
		self.logger.debug("Input --- createInput --- End")


	def startInput(self):
		self.logger.debug("Input --- startInput --- Start")
		self.logger.debug("Input --- startInput --- End")


	def stopInput(self):
		self.logger.debug("Input --- stopInput --- Start")
		self.logger.debug("Input --- stopInput --- End")


	def deleteInput(self):
		self.logger.debug("Input --- deleteInput --- Start")
		self.logger.debug("Input --- deleteInput --- End")