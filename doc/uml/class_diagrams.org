* List of Modules
  1. Input
  2. Processing
  3. Debugger
  4. Output
  5. Logger
  6. Exception
  7. Utility
* List of Class Diagrams
  1. Config Class
  2. Plugin Class
  3. 
** Use cases
*** Use Case Diagram
#+begin_src plantuml :file use_case_01.png
@startuml
left to right direction
skinparam packageStyle rectangle
actor User
rectangle Plugin {
left to right direction
  User --> (Input)
  (Input) --> (Process)
  (Process) --> (Output)
}
@enduml
#+end_src

#+RESULTS:
[[file:use_case_01.png]]

** Class Diagrams
*** Modules
@startuml
component Input
component Process
component Output
component Debug
component Logger
component Exception
component Utility
@enduml

*** Class Input
 #+begin_src plantuml :file class_input.png
 class Input {
   {method} createInput
   {method} startInput
   {method} stopInput
   {method} deleteInput
 }
 #+end_src 

 #+RESULTS:
 [[file:class_input.png]]

*** Class Process
 #+begin_src plantuml :file class_process.png
 class Process {
   {method} createProcess
   {method} startProcess
   {method} stopProcess
   {method} deleteProcess
 }
 #+end_src 

 #+RESULTS:
 [[file:class_process.png]]

*** Class Output
 #+begin_src plantuml :file class_output.png
 class Output {
   {method} createOutput
   {method} startOutput
   {method} stopOutput
   {method} deleteOutput
 }
 #+end_src 

 #+RESULTS:
 [[file:class_output.png]]

*** Class Debug
 #+begin_src plantuml :file class_debug.png
 class Debug {
   {method} createDebug
   {method} startDebug
   {method} stopDebug
   {method} deleteDebug
 }
 #+end_src 

 #+RESULTS:
 [[file:class_debug.png]]

*** Class Plugin
 #+begin_src plantuml :file class_plugin.png
 class Plugin {
   {field} Configuration
   {method} createFlow
   {method} startFlow
   {method} stopFlow
   {method} deleteFlow
 }
 #+end_src 

 #+RESULTS:
 [[file:class_plugin.png]]

*** Configuration Class
 #+begin_src plantuml :file class_configuration.png
 class Configuration {
   {field} data
   {method} readConfig
   {method} writeConfig
   {method} updateConfig
   {method} deleteConfig
   {method} getConfig
 }
 #+end_src 

 #+RESULTS:
 [[file:class_configuration.png]]

 #+begin_src plantuml :file class_log_manager.png
 class LogManager {
   {field} LogHandle
   {method} createLogger
   {method} deleteLogger
   {method} getLogHandler
   {method} setLogHandler
 }
 #+end_src 

 #+RESULTS:
 [[file:class_log_manager.png]]

 #+begin_src plantuml :file test.png
 class Dummy {
   {field} A field (despite parentheses)
   {method} Some method
 }
 #+end_src 

 #+begin_src plantuml :file test.png
 class Dummy {
   {field} A field (despite parentheses)
   {method} Some method
 }
 #+end_src 

 #+begin_src plantuml :file test.png
 class Dummy {
   {field} A field (despite parentheses)
   {method} Some method
 }
 #+end_src 

 #+begin_src plantuml :file test.png
 class Dummy {
   {field} A field (despite parentheses)
   {method} Some method
 }
 #+end_src 

 #+RESULTS:
 [[file:test.png]]
* BrandWatch Example
** Data Object
